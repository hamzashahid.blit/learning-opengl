#!/bin/sh

# Run the cmake command, -D (no space after) will take an option and set it accordingly
# Check the options at external/glfw/CMakeLists.txt Line 25
cmake -DGLFW_BUILD_DOCS=OFF . -B out/build
