#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <LearningOpenGLConfig.h>

int main(int argc, char* argv[]){

    /* Create a windowed mode window and its OpenGL context */

	std::cout << "Hello there! You are using: " << argv[0] << " Version " << LearningOpenGL_VERSION_MAJOR << "." << LearningOpenGL_VERSION_MINOR << "\n";



	GLFWwindow* window;

    /* Initialize the library */
	if(!glfwInit())
		return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    // glClearColor(0.4f, 0.3f, 0.4f, 0.0f);

	// float positions[6] = {
	// 	-0.5f, -0.5f,
	// 	 0.0f,  0.5f,
	// 	 0.5f, -0.5f
	// };

	// unsigned int buffer_id;
	// glGenBuffers(1, &buffer_id);
	// glBindBuffer(GL_ARRAY_BUFFER, buffer_id);
	// glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), positions, GL_STATIC_DRAW);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);

		// glDrawArrays(GL_TRIANGLES, 0, 3);

		glBegin(GL_TRIANGLES);
		glVertex2f(-0.5f, -0.5f);
		glVertex2f( 0.0f,  0.5f);
		glVertex2f( 0.5f, -0.5f);
		glEnd();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
