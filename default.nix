{ pkgs ? import <nixpkgs> {}, ... }: (
  pkgs.stdenv.mkDerivation {
	pname = "learning-opengl";
	version = "0.1";

	meta.mainProgram = "LearningOpenGL";

	# src = builtins.fetchGit {
	# 	url = "https://gitlab.com/hamzashahid.blit/learning-opengl.git";
	# 	ref = "refs/heads/master";
	# };

	src = ./.;

	buildInputs = with pkgs; [ gcc gdb cmake xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXi xorg.libXext libGL glfw glew ];

	configurePhase = ''
		substituteInPlace ./CMakeLists.txt \
			--replace "add_subdirectory(external/glfw)" \
						"#add_subdirectory(external/glfw)"

		substituteInPlace ./CMakeLists.txt \
			--replace "PUBLIC external/glfw/include" \
						"#PUBLIC external/glfw/include"

		substituteInPlace ./CMakeLists.txt \
			--replace "PRIVATE external/glfw/src" \
						"#PRIVATE external/glfw/src"

		substituteInPlace ./CMakeLists.txt \
			--replace "message(FATAL_ERROR \"The GLFW submodules" \
						"message(STATUS \"The GLFW submodules"

		./configure.sh
	'';

	buildPhase = ''
		./build.sh
	'';

	installPhase = ''	
		mkdir -p $out/bin	
		cp out/build/LearningOpenGL $out/bin
	'';
})
